<map version="0.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Forklift and OFork" ID="Freemind_Link_680768860">
<hook NAME="MapStyle" max_node_width="600">
    <properties show_notes_in_map="true"/>
<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="20"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="12"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right"/>
<stylenode LOCALIZED_TEXT="styles.topic" POSITION="right" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" POSITION="right" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" POSITION="right" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important" POSITION="right">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Forklift" POSITION="right" ID="ID_845996218">
<node TEXT="v. 0.3" ID="ID_396803431">
<node TEXT="Todo" ID="ID_1635207495">
<node TEXT="Features" ID="ID_1715458803">
<icon BUILTIN="idea"/>
</node>
<node TEXT="Issues" ID="ID_1993936830">
<icon BUILTIN="yes"/>
</node>
</node>
<node TEXT="Done" ID="ID_505859385">
<icon BUILTIN="button_ok"/>
<node TEXT="Many basic words are covered" ID="ID_83896204"/>
<node TEXT="Check the local variables/require issue" ID="ID_1905113639"/>
<node TEXT="free" ID="ID_400562771"/>
</node>
<node TEXT="Rejected" ID="ID_1267096484">
<icon BUILTIN="closed"/>
<node TEXT="Solve variables/closures issue" ID="ID_165219834">
<node TEXT="Possibly in a generalized form" ID="ID_1039258597"/>
<node TEXT="Ultimately, it means &quot;treat locals as globals&quot;, which might be exactly what we want for actual globals" ID="ID_1699181161"/>
</node>
</node>
</node>
<node TEXT="v 0.4" ID="ID_1446638211">
<node TEXT="Todo" ID="ID_543179015">
<node TEXT="Features" ID="ID_318136957">
<icon BUILTIN="idea"/>
<node TEXT="! word" ID="ID_1195909754">
<node TEXT="(or rename)" ID="ID_1673567620">
<node TEXT="take address of word" ID="ID_1189764459"/>
</node>
</node>
<node TEXT="Finish memory management" ID="ID_1775907836">
<node TEXT="defragment upon free&apos;ing" ID="ID_1246799344"/>
<node TEXT="defragment upon require&apos;ing" ID="ID_1878577363"/>
</node>
<node TEXT="Add tests for all existing words" ID="ID_580819923"/>
<node TEXT="Add still missing necessary words" ID="ID_39382531">
<node TEXT="asm" ID="ID_756031054">
<node TEXT="file and screen IO" ID="ID_1708293966">
<node TEXT="file io" ID="ID_946724106">
<node TEXT="fopen" ID="ID_1979300494"/>
<node TEXT="fread" ID="ID_1322937510"/>
<node TEXT="$fread" ID="ID_1900498291"/>
<node TEXT="fwrite" ID="ID_623888485"/>
<node TEXT="$fwrite" ID="ID_1273818780"/>
<node TEXT="fclose" ID="ID_518775425"/>
</node>
<node TEXT="screen io" ID="ID_849985166">
<node TEXT="getchar" ID="ID_1261200660"/>
<node TEXT="readline" ID="ID_477191220"/>
</node>
</node>
<node TEXT="strings" ID="ID_506131496">
<node TEXT="$append" ID="ID_1735471450"/>
<node TEXT="$substr" ID="ID_541932846"/>
<node TEXT="$find" ID="ID_634688076"/>
</node>
<node TEXT="floats" ID="ID_1646720429"/>
</node>
<node TEXT="forklift" ID="ID_1790375651"/>
<node TEXT="?" ID="ID_1250367748">
<node TEXT="basic data structures" ID="ID_1466254715">
<node TEXT="structs" ID="ID_43375200"/>
<node TEXT="arrays" ID="ID_687038731"/>
<node TEXT="hashes" ID="ID_1655244418"/>
</node>
</node>
</node>
<node TEXT="In/Output of .fo" ID="ID_1030554352"/>
<node TEXT="Cleanup" ID="ID_417072323">
<node TEXT="make language more forth-like" ID="ID_1572855430">
<node TEXT="COMPILE should only compile one word at a time" ID="ID_1526152404"/>
<node TEXT="COMPILE should work on a local pointer instead of program_text" ID="ID_954403960"/>
<node TEXT="make internal statuses accessable via words" ID="ID_261761384"/>
</node>
<node TEXT="reduce program globals as much as possible" ID="ID_818315267">
<node TEXT="this will also help avoid the x86/84-on-mac issue!" ID="ID_1801026890"/>
</node>
</node>
</node>
<node TEXT="Issues" ID="ID_476253540">
<icon BUILTIN="yes"/>
<node TEXT="change semantic of how to distinguish global/local variables" ID="ID_1383860910"/>
<node TEXT="Check whether it makes sense to have globals work entirely differently from locals" ID="ID_833714204"/>
</node>
</node>
</node>
<node TEXT="v 1.0" ID="ID_1239535563">
<node TEXT="Todo" ID="ID_685460326">
<node TEXT="NECESSARY" ID="ID_877990442">
<icon BUILTIN="stop-sign"/>
</node>
<node TEXT="should be done" ID="ID_1220049370">
<icon BUILTIN="messagebox_warning"/>
</node>
<node TEXT="maybe" ID="ID_749285677">
<icon BUILTIN="bookmark"/>
<node TEXT="forward references" ID="ID_1304188775"/>
</node>
<node TEXT="Issues" ID="ID_694563651"/>
</node>
<node TEXT="Done" ID="ID_1464875378">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="Rejected" ID="ID_739672006">
<icon BUILTIN="closed"/>
<node TEXT="true closures" ID="ID_1781715961">
<node TEXT="can access their source context" ID="ID_278177003"/>
</node>
</node>
</node>
</node>
<node TEXT="Diary" POSITION="right" ID="ID_1073714014">
<node TEXT="10.06.11" OBJECT="org.freeplane.features.common.format.FormattedDate|2011-06-10T00:00+0200|date" ID="ID_1419871500">
<node TEXT="In the middle of doing memory management; it&apos;s still find-first-fitting, not find-best-fitting! (and possibly will stay that way)" ID="ID_1952440312"/>
<node TEXT="The variables issue bugs me - I meditate on possibly removing them altogether, since they might just be a burden on this level. How many &quot;globals&quot; might you actually have on the stack, realistically?" ID="ID_726764673"/>
</node>
</node>
<node TEXT="OFork" POSITION="left" ID="ID_1806620556">
<node TEXT="Grammar" ID="ID_740234656">
<richcontent TYPE="NOTE">
<html>
  <head>
    
  </head>
  <body>
    <p>
      interfaceDeclaration ::= 'interface' name { ... }
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;| 'interface' name isA name { ... }
    </p>
    <p>
      ;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</map>
