#include "forklift.h"


.section .text

.set link, start_END_OF_CONTROLFLOW_DICTIONARY


# ( w -- w w )
declare_word DUP, "dup", 3                      # ( w -- w w )
    popq %rax
    pushq %rax
    pushq %rax
    jmp NEXT

# ( w1 w2 -- w2 w1 )
declare_word SWAP, "swap", 4                    # ( w1 w2 -- w2 w1 )
    popq %rax
    popq %rbx
    pushq %rax
    pushq %rbx
    jmp NEXT

# ( w -- )
declare_word DROP, "drop", 4                    # ( w -- )
    popq %rax
    jmp NEXT

# ( w1 w2 -- )
declare_word TWODROP, "2drop", 5                # ( w1 w2 -- )
    addq $2*SW, %rsp
    jmp NEXT

# ( w1 w2 -- w2 )
declare_word NIP, "nip", 3                      # ( w1 w2 -- w2 )
    popq %rax
    popq %rbx
    pushq %rax
    jmp NEXT

# ( a b c d -- c d )
declare_word TWONIP, "2nip", 4                  # ( a b c d -- c d )
    popq %rax
    popq %rbx
    popq %rcx
    popq %rcx
    pushq %rbx
    pushq %rax
    jmp NEXT

# (w -- w / w w)
declare_word QUESTIONDUP, "?dup", 4             # (w -- w / w w)
    movq (%rsp), %rax
    testl %eax, %eax
    jz NEXT
    pushq %rax
    jmp NEXT

# (w1 w2 -- w1 w2 w1 w2)
declare_word TWODUP, "2dup", 4                  # (w1 w2 -- w1 w2 w1 w2)
    movq (%rsp), %rbx
    movq SW(%rsp), %rax
    pushq %rax
    pushq %rbx
    jmp NEXT

# ( w1 w2 -- w1 w2 w1, copy 2nd word to top )
declare_word OVER, "over", 4                    # ( w1 w2 -- w1 w2 w1, copy 2nd word to top )
    movq SW(%rsp), %rax
    pushq %rax
    jmp NEXT

# ( w1 w2 w3 w4 w5 w6 -- w1 w2 w3 w4 w5 w6 w1 w2 )
declare_word TWOOVER, "2over", 5                # ( w1 w2 w3 w4 w5 w6 -- w1 w2 w3 w4 w5 w6 w1 w2 )
    movq 4*SW(%rsp), %rbx
    movq 5*SW(%rsp), %rax
    pushq %rax
    pushq %rbx
    jmp NEXT

    
# ( w1 w2 -- w2 w1 w2 )
declare_word TUCK, "tuck", 4                    # ( w1 w2 -- w2 w1 w2 )
    popq %rax
    popq %rbx
    pushq %rax
    pushq %rbx
    pushq %rax
    jmp NEXT

# ( a b c d -- c d a b c d )
declare_word TWOTUCK, "2tuck", 5                # ( a b c d -- c d a b c d )
    popq %rdx
    popq %rcx
    popq %rbx
    popq %rax
    pushq %rcx
    pushq %rdx
    pushq %rax
    pushq %rbx
    pushq %rcx
    pushq %rdx
    jmp NEXT

# ( w1 w2 w3 w4 -- w3 w4 w1 w2 )
declare_word TWOSWAP, "2swap", 5                # ( w1 w2 w3 w4 -- w3 w4 w1 w2 )
    pushq %rdx

    movq 4*SW(%rsp), %rax
    movq 3*SW(%rsp), %rbx
    movq 2*SW(%rsp), %rcx
    movq SW(%rsp), %rdx

    movq %rax, 2*SW(%rsp)
    movq %rbx, SW(%rsp)
    movq %rcx, 4*SW(%rsp)
    movq %rdx, 3*SW(%rsp)

    popq %rdx
    jmp NEXT

# ( a b c -- b c a, rotate third position to top )
declare_word ROT, "rot", 3                      # ( a b c -- b c a, rotate third position to top )
    popq %rcx
    popq %rbx
    popq %rax
    pushq %rbx
    pushq %rcx
    pushq %rax
    jmp NEXT

# ( a b c -- c a b, rotate top to third position )
declare_word MINUSROT, "-rot", 4                # ( a b c -- c a b, rotate top to third position )
    popq %rcx
    popq %rbx
    popq %rax
    pushq %rcx
    pushq %rax
    pushq %rbx
    jmp NEXT

# ( wn w... w0 n -- wn w... w0 wn, copy nth word to top)
declare_word PICK, "pick", 4                    # ( wn w... w0 n -- wn w... w0 wn, copy nth word to top)
    popq %rbx
    xorq %rax, %rax 
    movl %ebx, %eax
    movq (%rsp, %rax, SW), %rax
    pushq %rax
    jmp NEXT

# ( wn w... w0 n -- w ... w0 wn, move nth word to top )
declare_word ROLL, "roll", 4                    # ( wn w... w0 n -- w ... w0 wn, move nth word to top )
    popq %rax           # n
    testl %eax, %eax
    jz NEXT
    movq $0xffffffff, %rbx
    andq %rbx, %rax
    movq $0xffffffff, %rbx
    andq %rbx, %rcx
    movq (%rsp, %rax, SW), %rbx    # this is the rolled value, preserve!
    # movl $SW, %ecx
    # mull %ecx
roll_loop:
    movl %eax, %ecx
    decl %ecx
    movq (%rsp, %rcx, SW), %rcx
    movq %rcx, (%rsp, %rax, SW)
    decl %eax
    testl %eax, %eax
    jnz roll_loop
    movq %rbx, (%rsp)
    
    jmp NEXT

# ( wn ... w0 n -- wn w0 ... w1 )
# ( ex: 1 2 3    1 roll -> 1 3 2, 2 roll -> 3 1 2 )
declare_word MINUSROLL, "-roll", 5              # ( wn ... w0 n -- wn w0 ... w1 ) # ( ex: 1 2 3    1 roll -> 1 3 2, 2 roll -> 3 1 2 ) NOT CODED YET
    popq %rax

    pushq %rdi
    testl %eax, %eax
    jz NEXT
    movq $0xffffffff, %rbx
    andq %rbx, %rax
    movq $0xffffffff, %rbx
    andq %rbx, %rcx
    imull $SW, %eax
    addl $4, %eax
    xorl %ecx, %ecx
    addl $4, %ecx
    movl %ecx, %edx
    movl 4(%esp), %ebx    # this is the rolled value, preserve!
mr_loop:
    # .abort "NO IA64 version coded yet!!!"
    addl $SW, %edx
    movl (%esp, %edx, 1), %edi                      # TODO duh! this is no sensible way of addressing things
    movl %edi, (%esp, %ecx, 1)
    addl $SW, %ecx
    cmpl %ecx, %eax
    ja mr_loop

    movl %ebx, (%esp, %ecx, 1)

    popq %rdi
    jmp NEXT


# ( [s] -- )
declare_word CLEAR, "clear", 5                  # ( [s] -- )
    movq start_of_stack, %rsp
    jmp NEXT

# .s - nondestructively print the stack
# TODO: release the required data (for int_to_string and for fstring_to_cstring, respectively)
declare_word DOT_STACK, ".stack", 6             # .stack - nondestructively print the stack
    pushq %rbp
    movq %rsp, %rbp
    pushq %rax
    pushq %rbx
    pushq %rcx
    pushq %rdx                  # needs to be subtracted later!
    pushq %rsi
    pushq %rdi

    movq %rsp, %rsi
    addq $7*SW-SW, %rsi         # rsi is our "TOS"

    movq start_of_stack, %rdi   # rdi will be our pointer into the stack
    subq $SW, %rdi

dots_loop:
    cmpq %rdi, %rsi
    je dots_end

    movl (%rdi), %eax                # content of stack-pointer to eax, only 4 bytes!
    movl $tmp_buffer, %ebx             # string to target for int-to-string - TODO make this local
    call internal_int_to_string

    movl $tmp_buffer, %ebx           # add ' '
    movl (%ebx), %eax
    addl (%ebx), %ebx
    addl $4, %ebx
    movb $' ', (%ebx)
    incl %eax
    movl %eax, tmp_buffer


    # output 
    movl tmp_buffer, %edx           # length
    movl $tmp_buffer, %ecx
    addl $4, %ecx               # move pointer to start of string
    movl $1, %ebx               # filehandle stdout
    movl $SYS_WRITE, %eax       # syscall
    int $LINUX_SYSCALL          # call linux

    subq $SW, %rdi
    jmp dots_loop

dots_end:
    movl $2, %edx
    movl $.dots_msg, %ecx
    movl $1, %ebx               # filehandle stdout
    movl $SYS_WRITE, %eax       # syscall
    int $LINUX_SYSCALL          # call linux

    popq %rdi
    popq %rsi
    popq %rdx                 
    popq %rcx                  
    popq %rbx
    popq %rax                   
    popq %rbp

    jmp NEXT

.dots_msg: .ascii ". "

declare_word DEPTH, "depth", 5
    movq start_of_stack, %rax
    subq %rsp, %rax
    xorl %edx, %edx
    movl $SW, %ebx
    divl %ebx
    pushq %rax

    jmp NEXT

declare_word TO_RSTACK, ">r", 2
    popq %rax
    RPUSH eax
    jmp NEXT

declare_word FROM_RSTACK, "r>", 2
    RPOP eax
    pushq %rax
    jmp NEXT

declare_anonymous_word END_OF_STACK_DICTIONARY
