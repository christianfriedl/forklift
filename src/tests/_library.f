var current-test-name

: success
    "SUCCESS: " $. $. cr
;

: fail
    "FAIL: " $. $. cr
;

: assert ( success/bool message/string )
    swap 
    if 
        "SUCCESS: " $. $. cr
    else 
        "--> FAILURE: " $. $. " <--" $. cr
        "Stack: " $. .stack cr
    then
;

: current-test
    current-test-name :=
    "TEST " $. current-test-name ? $. "..." $. cr
;
