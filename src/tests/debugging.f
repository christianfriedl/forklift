: stack-clear-colon 1 drop ;
: stack-1-colon 1 ;
: stack-2-colon 1 2 ;
: stack-minus-1-colon drop ;
: give-okay-message if "stackframe ok" $. cr else "stackframe wrong" $. cr then ;

"right colon, difference 0: " $. {{ stack-clear-colon }} give-okay-message 
"wrong colon, difference not 0: " $. {{ stack-1-colon }} give-okay-message 
"wrong colon, difference not 0: " $. {{ stack-2-colon }} give-okay-message 

"right colon, difference 1: " $. {{ stack-1-colon 1 }}n give-okay-message
"wrong colon, difference not 1: " $. {{ stack-clear-colon 1 }}n give-okay-message
"wrong colon, difference not 1: " $. {{ stack-2-colon 1 }}n give-okay-message

"right colon, difference -1: " $. {{ stack-minus-1-colon -1 }}n give-okay-message
"wrong colon, difference not -1: " $. {{ stack-clear-colon -1 }}n give-okay-message
"wrong colon, difference not -1: " $. {{ stack-2-colon -1 }}n give-okay-message


"Aborting now!" abort

0 end
