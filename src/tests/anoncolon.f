: normal-colon "normalcolon works" $. cr ;
: colon-with-anon-colon 
    "here comes an anon colon: " $. 
    !: 
        1+ 
    !; 
    dup 
    . cr 
;

: colon-that-yields
    "colon-that-yields!!!" $. cr
    "here is a 1: " $. 1 dup . cr
    " ... and I yield... : " $.
    swap yield
    . cr
;

normal-colon
colon-with-anon-colon ( -- anon-colon-address )
colon-that-yields

0 end
