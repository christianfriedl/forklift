#include"_library.f"

: test-intarith-operators "test-intarith-operators" current-test
    8 2 *       16 = "*" assert
    -8 2 *      -16 = "* neg." assert
    2 2 +       4 = "+" assert
    -2 -2 +     -4 = "+ neg." assert
    5 3 -       2 = "- (first larger)" assert
    3 5 -       -2 = "- (second larger)" assert
    18 3 /      6 = "/" assert
    18 -3 /     -6 = "/ neg (1)." assert
    -18 3 /     -6 = "/ neg (2)." assert
    -18 -3 /     6 = "/ neg (3)." assert
    18 3 /mod   0 = swap 6 = and "/mod without remainder" assert
    19 4 /mod   3 = swap 4 = and "/mod with remainder" assert
    18 -3 /mod  0 = swap -6 = and "/mod without remainder, neg. (1)" assert
    -18 3 /mod  0 = swap -6 = and "/mod without remainder, neg. (2)" assert
    -18 -3 /mod  0 = swap 6 = and "/mod without remainder, neg. (3)" assert
    -17 3 /mod  -2 = swap -5 = and "/mod with remainder, neg." assert
    2 6 3 */    4 = "*/" assert
    1 2 max     2 = "max" assert
    -12 -5 max  -5 = "max neg." assert
    1 2 min     1 = "min" assert
    -12 -5 min  -12 = "min" assert
    8 1+        9 = "1+" assert
    8 1-        7 = "1-" assert
    -24 abs     24 = "abs(-)" assert
    25 abs      25 = "abs(+)" assert
    0 abs       0 = "abs(0)" assert
    15 2*       30 = "2*" assert
    16 2/       8 = "2/" assert
    1 5 <<      32 = "<<" assert
    32 5 >>     1 = ">>" assert
    257 -1*     -257 = "-1*" assert
    -257 -1*    257 = "-1* neg." assert
;

test-intarith-operators

0 end

