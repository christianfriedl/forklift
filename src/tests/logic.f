#include"_library.f"

: test-true " test-true" current-test
    true -1 = "true = -1" assert
    false 0 = "false = 0" assert
;

: test-logic-not " test-logic-not" current-test
    true not false = "not true" assert
    false not true = "not false" assert
;

: test-logic-and " test-logic-and" current-test
    true true and true = "true and true" assert
    true false and false = "true and false" assert
    false true and false = "false and true" assert
    false false and false = "false and false" assert
;

: test-logic-or " test-logic-or" current-test
    true true or true = "true or true" assert
    true false or true = "true or false" assert
    false true or true = "false or true" assert
    false false or false = "false or false" assert
;

: test-logic-xor " test-logic-xor" current-test
    true true xor false = "true xor true" assert
    true false xor true = "true xor false" assert
    false true xor true = "false xor true" assert
    false false xor false = "false xor false" assert
;

: test-bin-not " test-bin-not" current-test
    true ! false = "! true" assert
    false ! true = "! false" assert
;

: test-bin-and " test-bin-and" current-test
    0 0 & 0 = "0 & 0 = 0" assert
    1 0 & 0 = "1 & 0 = 0" assert
    1 1 & 1 = "1 & 1 = 1" assert
    1 2 & 0 = "1 & 2 = 0" assert
    1 3 & 1 = "1 & 3 = 1" assert
;

: test-bin-or " test-bin-or" current-test
    0 0 | 0 = "0 | 0 = 0" assert
    1 0 | 1 = "1 | 0 = 1" assert
    1 1 | 1 = "1 | 1 = 1" assert
    1 2 | 3 = "1 | 2 = 3" assert
    1 3 | 3 = "1 | 3 = 3" assert
;

: test-bin-xor " test-bin-xor" current-test
    0 0 ^ 0 = "0 ^ 0 = 0" assert
    1 0 ^ 1 = "1 ^ 0 = 1" assert
    1 1 ^ 0 = "1 ^ 1 = 0" assert
    1 2 ^ 3 = "1 ^ 2 = 3" assert
    1 3 ^ 2 = "1 ^ 3 = 2" assert
;

: test-relative-operators " test-relative-operators" current-test
    1 1 = true = "1 = 1" assert
    1 0 = false = "1 = 0" assert

    1 1 <> false = "1 <> 1" assert
    1 0 <> true = "1 <> 0" assert

    1 0 > true = "1 > 0" assert
    1 1 > false = "1 > 1" assert
    0 1 > false = "0 > 1" assert

    1 0 < false = "1 < 0" assert
    1 1 < false = "1 < 1" assert
    0 1 < true = "0 < 1" assert

    1 0 >= true = "1 >= 0" assert
    1 1 >= true = "1 >= 1" assert
    0 1 >= false = "0 >= 1" assert

    1 0 <= false = "1 <= 0" assert
    1 1 <= true = "1 <= 1" assert
    0 1 <= true = "0 <= 1" assert

    0 0= true = "0 0=" assert
    1 0= false = "1 0=" assert

    1 0< false = "1 0<" assert
    0 0< false = "0 0<" assert
    -1 0< true = "-1 0<" assert

    1 0> true = "1 0>" assert
    0 0> false = "0 0>" assert
    -1 0> false = "-1 0>" assert

    0 
;




test-true

test-logic-not
test-logic-and
test-logic-or
test-logic-xor

test-bin-not
test-bin-and
test-bin-or
test-bin-xor

test-relative-operators

0 end
