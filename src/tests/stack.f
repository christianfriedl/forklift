#include"_library.f"

: test-basic-stack-words " test-basic-stack-words" current-test
    {{
        1 2 3 depth 3 = "depth correct after pushes" assert
        clear
    }} "stack correctness" assert
    {{
        1 2 3
        dup 3 = swap 3 = and "dup" assert
        clear 1 2 3
        swap 2 = swap 3 = and "swap" assert
        clear 1 2 3
        drop 2 = swap 1 = and "drop" assert
        clear
    }} "stack correctness" assert
;

: test-double-basic-stack-words " test-double-basic-stack-words" current-test
    2drop
    2dup
;

: test-rstack-words " test-rstack-words" current-test
    {{
        1 2 3 >r depth 2 = "depth correct after >r" assert
        r> dup 3 = "correctly re-read from returnstack" assert
        depth 3 = "depth correct after r>" assert
        clear
    }} "stack correctness" assert
;

test-basic-stack-words
test-rstack-words

0 end
