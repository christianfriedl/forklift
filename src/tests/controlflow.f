#include "_library.f"

: test-function-call "test-function-call" current-test
    1 "function called" assert
;

: test-if-then "test-if-then" current-test
    0 if 0 "0=false" assert then
    1 if 1 "1=true" assert then
;

: test-if-else-then "test-if-else-then" current-test
    0 if 0 else 1 then 1 = "0=false" assert
    1 if 1 else 0 then 1 = "1=true" assert
;

: test-begin-while-repeat "test-begin-while-repeat" current-test
    0 begin dup 10 < while 1+ repeat
    10 = "loop repeats 10 times" assert
;

: test-begin-until "test-begin-until" current-test
    0 begin 1+ dup 10 = until
    10 = "loop repeats 10 times" assert
;

test-function-call

test-if-then
test-if-else-then

test-begin-while-repeat
test-begin-until

0 end

