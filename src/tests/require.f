#include"_library.f"

var reference

: test-require-minimal
    4 require
    dup 0 <> "require returns non-null value" assert
    free
;

: test-write-to-required-space
    4 require
    dup 12345 swap :=
    free
;

: test-write-and-read
    4 require
    dup 12345 swap :=
    dup ? 12345 = "require returns written value" assert
    free
;

: test-write-and-read-with-local-variable-as-reference
    var reference
    4 require
    reference :=
    12345 reference ? :=
    reference ? ? 12345 = "local reference returns written value" assert
;

: test-write-and-read-with-global-variable-as-reference
    4 require
    reference :=
    12345 reference ? := 
    reference ? ? 12345 = "global reference returns written value" assert
;


test-require-minimal
test-write-to-required-space
test-write-and-read
test-write-and-read-with-global-variable-as-reference
test-write-and-read-with-local-variable-as-reference

0 end
