#include"_library.f"
var global-variable 

: test-local-variable
    var local-variable
    local-variable 0 <> if "local-variable exists." success else "local-variable does not exist" fail then

    1 local-variable :=
    local-variable ? 
    1 = if "local-variable is set correctly" success else "local-variable is not set correctly" fail then
;

: test-global-variable-from-colon
    1 global-variable :=
    global-variable ?
    1 = if "global-variable is set correctly inside colon" success else "global-variable is not set correctly inside colon" fail then
;

: test-assign-reference
    256 require
    dup global-variable :=
    global-variable ? 
    = if "global-variable is set correctly to reference" success else "global-variable is not set correctly to reference" fail then
    65536 global-variable ? := 
    global-variable ? ? 65536 = "retrieving global-variable" assert
;

test-local-variable

global-variable 0 <> if "global-variable exists." success else "global-variable does not exist" fail then

1 global-variable :=
global-variable ? 
1 = if "global-variable is set correctly" success else "global-variable is not set correctly" fail then

test-global-variable-from-colon
test-assign-reference


0 end
