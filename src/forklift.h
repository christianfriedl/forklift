#include "syscall.inc"

#define SW 8
#define SOURCEFILE_LENGTH 262144
#define EXECFILE_LENGTH 262144

/* TODO:
    Wordtypes into dictionary
    Link Var_dictionary before current function (remove rpush/rpop)
    ... and later on, make global functions link before first colondef
    compile CALCVARADDR
    variables runtime stuff: CALCVARADDR
*/

#define STATUS_IMMEDIATE 1
#define STATUS_COMPILE 2
#define INT_SIGNBIT 2147483648
#define WORDTYPE_CODE 1
#define WORDTYPE_COLONDEF 2
#define WORDTYPE_VARIABLE 4
#define CALLTYPE_IMMEDIATE 8
#define CALLTYPE_RUNTIME 16
#define REFTYPE_INTERNAL 32
#define REFTYPE_EXTERNAL 64

/*
    macro declare_word
    used for declaring a word that will be used from forth
*/
.macro declare_word label, name, strlen, immediateflag=0

.globl start_\label
start_\label:
    .int \strlen          # strlen
    .ascii "\name"      # name
    .int link          # previous-ptr
    .set link, start_\label
    .byte \immediateflag
    .int 0                  # codelength

.globl \label
.type \label, @function
\label:

.endm

/*
    macro declare_anonymous_word
    used for declaring a word that is only used in the bootup process/ not used in forth (e.g. end of dictionary)
*/
.macro declare_anonymous_word label, immediateflag=0

.globl start_\label
start_\label:
    .int 0              # strlen
    .int link           # previous-ptr
    .set link, start_\label
    .byte \immediateflag
    .int 0                  # codelength

.globl \label
.type \label, @function
\label:

.endm

.macro RPUSH reg
    movl %\reg, (%edi)
    addl $4, %edi
.endm

.macro RPOP reg
    subl $4, %edi
    movl (%edi), %\reg
.endm

