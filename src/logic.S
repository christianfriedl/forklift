#include "forklift.h"

.section .text

.set link, start_END_OF_STRING_DICTIONARY

declare_word TRUE, "true", 4
    movl $-1, %eax
    pushq %rax
    jmp NEXT

declare_word FALSE, "false", 5
    xorl %eax, %eax
    pushq %rax
    jmp NEXT

declare_word LOGIC_NOT, "not", 3 
    popq %rax
    testl %eax, %eax
    jz not_zero     # duh! i'm sure there's a WAY better method!
    pushq $0
    jmp NEXT
not_zero:
    pushq $-1
    jmp NEXT

declare_word LOGIC_AND, "and", 3 
    popq %rax
    popq %rbx
    cmpl $0, %eax
    je land_false
    cmpl $0, %ebx
    je land_false
    pushq $-1
    jmp NEXT
land_false:
    pushq $0
    jmp NEXT

declare_word LOGIC_OR, "or", 2
    popq %rax
    popq %rbx
    cmpl $0, %eax
    jne lor_true
    cmpl $0, %ebx
    jne lor_true
    pushq $0
    jmp NEXT
lor_true:
    pushq $-1
    jmp NEXT

declare_word LOGIC_XOR, "xor", 3
    popq %rax
    popq %rbx

    cmpl $0, %eax
    je lxor_a_0
    movl $-1, %eax

lxor_a_0:
    cmpl $0, %ebx
    je lxor_b_0
    movl $-1, %ebx

lxor_b_0:
    
    xorl %eax, %ebx
    pushq %rbx

    jmp NEXT

declare_word BIN_NOT, "!", 1        # ( one's complement -> i -- !i )
    popq %rax

    notl %eax

    pushq %rax
    jmp NEXT


declare_word BIN_AND, "&", 1
    popq %rax
    popq %rbx

    andl %eax, %ebx

    pushq %rbx
    jmp NEXT

declare_word BIN_OR, "|", 1
    popq %rax
    popq %rbx

    orl %eax, %ebx

    pushq %rbx
    jmp NEXT

declare_word BIN_XOR, "^", 1
    popq %rax
    popq %rbx

    xorl %eax, %ebx

    pushq %rbx
    jmp NEXT


declare_word EQ, "=", 1
    popq %rax
    popq %rbx
    cmpl %eax, %ebx
    je eq_true
    pushq $0
    jmp NEXT
eq_true:
    pushq $-1
    jmp NEXT

declare_word NE, "<>", 2
    popq %rax
    popq %rbx
    cmpl %eax, %ebx
    je ne_false
    pushq $-1
    jmp NEXT
ne_false:
    pushq $0
    jmp NEXT

declare_word GT, ">", 1
    popq %rax
    popq %rbx
    cmpl %eax, %ebx
    jg gt_true
    pushq $0
    jmp NEXT
gt_true:
    pushq $-1
    jmp NEXT

declare_word LT, "<", 1
    popq %rax
    popq %rbx
    cmpl %eax, %ebx
    jl lt_true
    pushq $0
    jmp NEXT
lt_true:
    pushq $-1
    jmp NEXT

declare_word GE, ">=", 2 
    popq %rax
    popq %rbx
    cmpl %eax, %ebx
    jge ge_true
    pushq $0
    jmp NEXT
ge_true:
    pushq $-1
    jmp NEXT

declare_word LE, "<=", 2
    popq %rax
    popq %rbx
    cmpl %eax, %ebx
    jle le_true
    pushq $0
    jmp NEXT
le_true:
    pushq $-1
    jmp NEXT

declare_word ZEQ, "0=", 2
    popq %rax
    testl %eax, %eax
    jz zeq_zero
    pushq $0
    jmp NEXT
zeq_zero:
    pushq $-1
    jmp NEXT

declare_word ZLT, "0<", 2
    popq %rax
    cmpl $0, %eax
    jl zlt_less
    pushq $0
    jmp NEXT
zlt_less:
    pushq $-1
    jmp NEXT

declare_word ZGT, "0>", 2 
    popq %rax
    cmpl $0, %eax
    jle zgt_less
    pushq $-1
    jmp NEXT
zgt_less:
    pushq $0
    jmp NEXT

declare_anonymous_word END_OF_LOGIC_DICTIONARY

