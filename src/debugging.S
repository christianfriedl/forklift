#include"forklift.h"

.set link, start_END_OF_COMMANDLINE_DICTIONARY

/*
    abort with error message
    ( fstring -- )
*/
declare_word ABORT, "abort", 5                      # ( abort the program with a message -> fstring -- )
    jmp ENTER
    .hword 0                        # count of local variables
    .int LITERAL
    .int .ABORT_message
    .int STRINGDOT
    .int STRINGDOT
    .int LITERAL
    .int 1
    .int END
    .int EXIT
    
.ABORT_message:
    .int 10
    .ascii "Aborting: "


/*
    check whether the stack is in the state we want it   
*/
declare_word SETBASE, "{{", 2                       # ( set the stack frame for testing -> -- )
    movq %rsp, %rbp
    jmp NEXT

declare_anonymous_word CHECKBASE_internal
    subq %rsp, %rbp
    pushq %rbp
    jmp NEXT


/*
    }} - basic CHECKBASE: will push 0 for error, 1 for ok
*/
declare_word CHECKBASE, "}}", 2                     # ( check the stack frame -> -- bool)
    jmp ENTER
    .hword 0
    .int CHECKBASE_internal

    .int LOGIC_NOT

    .int EXIT

declare_anonymous_word CHECKBASE_with_number_internal
    popq %rax                               # expected difference
    imull $SW, %eax
    subq %rsp, %rbp
    subq %rax, %rbp
    pushq %rbp
    jmp NEXT

declare_word CHECKBASE_with_number, "}}n", 3
    jmp ENTER
    .hword 0

    .int CHECKBASE_with_number_internal

    .int LOGIC_NOT

    .int EXIT


declare_anonymous_word END_OF_DEBUGGING_DICTIONARY

/***********************************

    END OF ALL DICTIONARIES

 **********************************/
.globl after_dictionary
after_dictionary: .int .

