#include "forklift.h"

.section .text

.set link, start_END_OF_STACK_DICTIONARY



/*
    $COPY
    ( source target -- )
*/
declare_word STRINGCOPY, "$copy", 5
    popq %rbx
    popq %rax
    call internal_stringcopy
    jmp NEXT



/*
    PARAMETERS: src f-string in eax; target f-string in ebx
        space needs to be already required for target string!

    TODO: use STOSL
*/

.globl internal_stringcopy
.type internal_stringcopy, @function

internal_stringcopy:
    pushq %rcx
    pushq %rdx

    xorq %rcx, %rcx
    movl (%rax), %ecx           # copy length
    addl $3, %ecx               # add length-field-1 (len/index) to length
internal_strcp_loop:
    movb (%rax, %rcx, 1), %dl
    movb %dl, (%ebx, %ecx, 1)
    loop internal_strcp_loop
    movb (%rax, %rcx, 1), %dl
    movb %dl, (%ebx, %ecx, 1)

    popq %rdx
    popq %rcx

    ret


/*
    returns a NEW cstring with the contents from the fstring
    PARAMETERS: %eax - src f-string
    RETURN: address of cstring in %eax
*/
.globl internal_fstring_to_cstring
.type internal_fstring_to_cstring, @function

internal_fstring_to_cstring:
    pushq %rbx
    pushq %rcx
    pushq %rdx

    movl %eax, %ebx             # save src address
    addl $4, %ebx
    movl (%eax), %eax           # length
    movl %eax, %ecx             # counter for the loop ahead
    incl %eax                   # \0 at end
    call internal_require             # dest address is now in %eax
    movb $0, (%eax, %ecx, 1)
oftc_loop:
    decl %ecx
    movb (%ebx, %ecx, 1), %dl
    movb %dl, (%eax, %ecx, 1)
    testl %ecx, %ecx
    jnz oftc_loop

    popq %rdx
    popq %rcx
    popq %rbx
    ret

declare_word STRING_TO_CSTRING, "$to_cstring", 11
    popq %rax
    call internal_fstring_to_cstring
    pushq %rax
    jmp NEXT


declare_word STRING_FROM_CSTRING, "$from_cstring", 13
    popq %rax
    call internal_fstring_from_cstring
    pushq %rax
    jmp NEXT

/*
    INPUT: source cstring in %rax
    OUTPUT: new fstring in %rax
*/
.globl internal_fstring_from_cstring
.type internal_fstring_from_cstring, @function
internal_fstring_from_cstring:
    pushq %rbx
    pushq %rcx
    pushq %rdx
    pushq %rsi
    pushq %rdi

    movq %rax, %rsi 
    movq %rax, %rdx                                 # save source for copying
    xorq %rcx, %rcx
internal_fstring_from_cstring_strlen_loop:
    lodsb
    testb %al, %al
    jz internal_fstring_from_cstring_after_strlen_loop
    incl %ecx
    jmp internal_fstring_from_cstring_strlen_loop

internal_fstring_from_cstring_after_strlen_loop:
    movl %ecx, %eax
    addl $4, %eax
    call internal_require
    xorq %rsi, %rsi
    xorq %rdi, %rdi

    movl %ecx, (%eax)
    movq %rdx, %rsi
    movl %eax, %edx                             # save dest for return
    movl %eax, %edi
    addl $4, %edi
internal_fstring_from_cstring_copy_loop:
    lodsb
    stosb
    loopnz internal_fstring_from_cstring_copy_loop

    movl %edx, %eax

    popq %rdi
    popq %rsi
    popq %rdx
    popq %rcx
    popq %rbx
    ret

# ( str-addr1 str-addr2 -- bool )
declare_word STRINGEQUAL, "$=", 2
    popq %rax
    popq %rbx
    call internal_strequal
    pushq %rax
    jmp NEXT

/*
    PARAMS: fstring *(eax), fstring *(ebx)
    RETURN: bool (0/1) found
*/
.globl internal_strequal
.type internal_strequal,@function
internal_strequal:
    pushq %rbx
    pushq %rcx
    pushq %rdx

    movl (%eax), %ecx
    cmpl %ecx, (%ebx)
    jne ose_uneq
    addl $4, %eax
    addl $4, %ebx
    decl %ecx
ose_loop:
    movb (%eax, %ecx, 1), %dl
    cmpb %dl, (%ebx, %ecx, 1)
    jne ose_uneq
    testl %ecx, %ecx
    jz ose_loop_end
    decl %ecx
    jmp ose_loop
ose_loop_end:

    movl $1, %eax
    jmp ose_ret

ose_uneq:
    xorl %eax,%eax
ose_ret:

    popq %rdx
    popq %rcx
    popq %rbx
    ret

declare_anonymous_word END_OF_STRING_DICTIONARY
