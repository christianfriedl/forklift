( *** CONWAY'S GAME OF LIFE                                             ***
      a test of many forth features, implementing conway's game of life 
)

variable counter

variable board ( the board is a simple flat array)
variable board2 ( for the next generation)

: rows 20 ; ( number of rows)
: columns 20 ; ( number of columns)

: rows 80 ; ( number of rows)
: columns 150 ; ( number of columns)

: rows 50 ; ( number of rows)
: columns 150 ; ( number of columns)

: getch_interval 10 ; ( number of iterations before we getch again )
: max_generations 10000 ; ( number of maximum iterations )

: dead_cell " " ;
: alive_cell "#" ;

( x-pos y-pos -- array-index )
: array_index_from_x_y 
    columns * + 
;

( translates an index to an element-address of the board)
( index board-ref -- element-addr)
: index_to_element
    ?                       ( index board-addr)
    swap                    ( board-addr index)
    []                      ( element-addr)
;

: init_board
    rows columns *         ( maxcount)
    dup [create board :=    ( create and set board from maxcount)
    rows columns *         ( maxcount)
    0                       ( maxcount i)
    begin
        2dup                ( maxcount i maxcount i)
    > while                 ( maxcount i)
        board ?             ( maxcount i board)
        over                ( maxcount i board i )
        dead_cell         ( maxcount i board i dead_cell)
        -rot                ( maxcount i dead_cell board i)
        []:=                ( maxcount i )
        1+
    repeat
    2drop drop
;

: clear_board2
    rows columns *         ( maxcount)
    0                       ( maxcount i)
    begin
        2dup                ( maxcount i maxcount i)
    > while                 ( maxcount i)
        dup                 ( maxcount i i)
        board2 index_to_element    ( maxcount i element-addr)
        dead_cell         ( maxcount i element-addr string-addr)
        swap                ( maxcount i string-addr element-addr)
        :=                  ( maxcount i)

        1+
    repeat
    2drop 
;

: init_board2
    rows columns *         ( maxcount)
    dup [create board2 :=    ( create and set board from maxcount)
    drop
;

( y -- )
: print_line
    columns                     ( y columns)
    0                           ( y columns i)
    begin
        2dup                    ( y columns i columns i)
    > while                     ( y columns i)
        2 pick                  ( y columns i y)
        over                    ( y columns i y i)
        swap                    ( y columns i i y)
        array_index_from_x_y    ( y columns i index)
        board index_to_element        ( y columns i element-addr)
        ?                       ( y columns i string-addr)
        $.                      ( y columns i)
        1+                      ( y columns i)
    repeat
    cr
    drop drop drop
;

: print_board
    rows                       ( rows)
    0                           ( rows i)
    begin
        2dup                    ( rows i rows i)
    > while                     ( rows i)
        dup                     ( rows i i)
        print_line              ( rows i)
        1+                      ( rows i)
    repeat
    2drop

    0
    begin
        dup columns <
    while
        "=" $.
        1+
    repeat
    cr
    drop
;


( x y string-addr -- )
: set_field
    -rot                        ( string-addr x y )
    array_index_from_x_y        ( string-addr index)
    board index_to_element            ( string-addr element-addr)
    :=
;
( x y string-addr -- )
: set_new_field
    -rot                        ( string-addr x y )
    array_index_from_x_y        ( string-addr index)
    board2 index_to_element            ( string-addr element-addr)
    :=
;

( x y -- bool:alive?)
: field_alive?
    array_index_from_x_y        ( index)
    board index_to_element            ( element)
    ?                           ( string)
    alive_cell                 ( string string)
    $=
;


( x y -- )
: increase_counter_if_alive
    field_alive?                ( bool:alive?)
    if                          ( counter-ref)
        counter ?               ( counter)
        1+                      ( counter)
        counter :=              ( )
    then
;

(  count alive fields above position)
( int:x int:y -- x y)
: count_alive_cells_above
    dup 0 > if
        over 0 >                    ( x y bool)
        if                          ( x y)
            2dup                        ( x y)
            1-                          ( x y-1)
            swap                        ( y-1 x)
            1-                          ( y-1 x-1)
            swap                        ( x-1 y-1)
            increase_counter_if_alive
        then

        2dup
        1-
        increase_counter_if_alive

        over columns 1- < if
            2dup
            1-
            swap
            1+
            swap
            increase_counter_if_alive
        then
    then
;

(  count alive fields below position)
( int:x int:y -- x y)
: count_alive_cells_below
    dup rows 1- < if 
        over 0 > if
            2dup                        ( x y)
            1+                          ( x y-1)
            swap                        ( y-1 x)
            1-                          ( y-1 x-1)
            swap                        ( x-1 y-1)
            increase_counter_if_alive
        then

        2dup
        1+
        increase_counter_if_alive

        over columns 1- < if
            2dup
            1+
            swap
            1+
            swap
            increase_counter_if_alive
        then
    then
;

( x y -- x y)
: count_alive_cells_same_line
    over 0 > if
        2dup                        
        swap                        
        1-                          
        swap                        
        increase_counter_if_alive
    then

    over columns 1- < if
        2dup                        
        swap                     
        1+                        
        swap                       
        increase_counter_if_alive
    then
;


( x y -- count )
: count_alive_cells
    0 counter :=
    count_alive_cells_above
    count_alive_cells_same_line
    count_alive_cells_below
    2drop
    counter ? 
;


( an already alive field survives)
( x y -- bool:survives?)
: field_survives? 
    count_alive_cells              ( counter )
    dup 2 >=                        ( counter bool)
    over 3 <=                       ( counter bool bool )
    and                             ( counter bool)
    swap drop                       ( bool )
;

( an empty field gets a new token)
( x y -- bool:new?)
: field_born?
    count_alive_cells              ( counter )
    3 =                             ( bool )
;

: calc_next_generation
    0                              ( y )
    begin
        dup                         ( y y )
        rows  <                  ( y )
    while
        0                           ( y x )
        begin
            dup
            columns <               ( y x )
        while
            swap
            2dup field_alive?
            if
                2dup field_survives?
                if 
                    2dup 
                    alive_cell set_new_field
                else
                    2dup 
                    dead_cell set_new_field
                then
            else
                2dup field_born?
                if
                    2dup
                    alive_cell set_new_field
                else
                    2dup
                    dead_cell set_new_field
                then
            then
            swap
            1+
        repeat
        drop                        ( y)
        1+
    repeat
    drop
;

: copy_next_generation_to_board
    0
    begin
        dup
        rows columns * <
    while
        board2                      ( index b2-ref)
        ?                           ( index b2)
        over                        ( index b2 index)
        []?                           ( index b2-string)
        over                        ( index b2-string index)
        board                       ( index b2-string index board-ref)
        ?                           ( index b2-string index board)
        swap                        ( index b2-string board index)
        []:=                          ( index )

        1+
    repeat
    drop

;
: get_center_x
    columns 2 /
; 

: get_center_y
    rows 2 /
; 

: setup_quadratl
    10 10 alive_cell set_field
    11 10 alive_cell set_field
    12 10 alive_cell set_field

    10 11 alive_cell set_field
    12 11 alive_cell set_field

    10 12 alive_cell set_field
    11 12 alive_cell set_field
    12 12 alive_cell set_field
;
: setup_glider 
    2 2 alive_cell set_field
    3 3 alive_cell set_field
    3 4 alive_cell set_field
    2 4 alive_cell set_field
    1 4 alive_cell set_field
;

: setup_diehard  ( - disappears after 130 generation )

    30 20 alive_cell set_field
    31 20 alive_cell set_field
    31 21 alive_cell set_field

    35 21 alive_cell set_field
    36 21 alive_cell set_field
    37 21 alive_cell set_field

    36 19 alive_cell set_field
;

: setup_acorn  ( - lives 5206 generations and generates gliders )
    get_center_x        get_center_y        alive_cell set_field
    get_center_x 1+     get_center_y        alive_cell set_field
    get_center_x 1+     get_center_y 2 -    alive_cell set_field
    get_center_x 3 +    get_center_y 1-     alive_cell set_field
    get_center_x 4 +    get_center_y        alive_cell set_field
    get_center_x 5 +    get_center_y        alive_cell set_field
    get_center_x 6 +    get_center_y        alive_cell set_field
;


: setup_initial_boards
    init_board
    init_board2
    setup_acorn
;

( MAIN )

start

setup_initial_boards

print_board
0
begin
    getch
    drop
    0 begin dup getch_interval < while
        calc_next_generation
        copy_next_generation_to_board
        1+
    repeat
    drop
    print_board
getch_interval +
"Generations: " $. dup . cr
dup max_generations >= until

end

