codeblock Curses

    : COLOR_BLACK	0 ;
    : COLOR_RED	    1 ;
    : COLOR_GREEN	2 ;
    : COLOR_YELLOW	3 ;
    : COLOR_BLUE	4 ;
    : COLOR_MAGENTA	5 ;
    : COLOR_CYAN	6 ;
    : COVLOR_WHITE	7 ;

    variable libcurses_handle

    : init_library
        RTLD_LAZY "/usr/lib/libncurses.so.5" dlopen libcurses_handle :=  
        ( 64bit version : RTLD_LAZY "/usr/lib64/libncurses.so" dlopen libcurses_handle := )
    ;

    : get_libcurses_handle
        libcurses_handle ?
    ;

    ( -- WINDOW*)
    : initscr
        "initscr" libcurses_handle ? dlsym 
        0 swap dlcall
    ;

    : clear
        "clear" libcurses_handle ? dlsym
        0 swap dlcall
        drop
    ;

    : refresh
        "refresh" libcurses_handle ? dlsym
        0 swap dlcall
        drop
    ;

    ( string -- )
    : addstr
        $>cstring
        "addstr" libcurses_handle ? dlsym       ( cstring symhandle)
        1 swap                                ( string symhandle argcount -- string argcount symhandle )
        dlcall
        drop
    ;

    ( string x y -- )
    : mvaddstr
        rot $>cstring -rot
        "mvaddstr" libcurses_handle ? dlsym         ( cstring x y symhandle)
        3 swap dlcall drop
    ;

    ( string x y num-additional-args-without-fixed -- )
    : mvprintw
        3 roll $>cstring 3 -roll
        3 +
        "mvprintw" libcurses_handle ? dlsym         ( string x y numargs symhandle)
        dlcall drop
    ;

    : getch
        "getch" libcurses_handle ? dlsym
        0 swap dlcall
    ;

    : endwin
        "endwin" libcurses_handle ? dlsym
        0 swap dlcall
        drop
    ;


    ( NOTE: this is a macro in ncurses.h! - lookup struct _win_st and getmaxy, getmaxx for details - y is at 4, x at 6)
    ( WINDOW* -- y x )
    : getmaxyx
         dup 4 + ? 65535 &  ( win y )
         swap 6 + ? 65535 &  ( y x )
    ;

    ( -- retval-of-startcolor )
    : start_color
        "endwin" libcurses_handle ? dlsym
        0 swap dlcall
    ;


