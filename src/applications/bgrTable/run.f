#include"../../tests/_library.f"
#include"bgrTable.f"

: printstack ( debugging OFF ) drop ;
: printstack ( debugging ON ) $. " - stack: " $. .stack cr ;
: }}nprint 
    }}n 
    if "stack ok " $. cr
    else "stack not ok " $. cr " -- " $. printstack
    then 
;

: printABgr
    bgrRow.getABgr .
;
: printDBgr
    bgrRow.getDBgr . 
;
: printUstProz
    bgrRow.getUstProz . 
;
: printRow
    " aBgr: " $. dup printABgr ", dBgr: " $. dup printDBgr ", ustProz: " $. printUstProz cr
;

20 bgrTable::new 
"table is at: " $. dup . cr

1 2 10 bgrRow::new
"1st row is at: " $. dup . cr

dup printRow

swap dup -rot

bgrTable.addRow

2 2 20 bgrRow::new
"2nd row is at: " $. dup . cr

dup printRow


swap dup -rot 


bgrTable.addRow 


dup 0 swap bgrTable.getRowByIndex
"fetched row 0 at " $. dup . cr
printRow

dup 1 swap bgrTable.getRowByIndex
"fetched row 1 at " $. dup . cr
printRow

0 end
