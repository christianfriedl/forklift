( holt die buchungsgruppe von einer tabelle und kalkuliert den bruttopreis aus dem nettopreis )

var bgrTable 

: reference::size 4 ;
: integer::size 4 ;

( array of bgrTableRows, das sind immer  aBgr, dBgr, ustProz )

: bgrRow::size integer::size 3 * ;

: bgrRow.getABgr ( bgrRow -- aBgr )
    ?
;
: bgrRow.setABgr ( aBgr bgrRow -- )
    :=
;

: bgrRow.getDBgr ( bgrRow -- dBgr )
    4 + ?
;

: bgrRow.setDBgr ( dBgr bgrRow -- )
    4 + :=
;

: bgrRow.getUstProz ( bgrRow -- ustProz )
    8 + ?
;

: bgrRow.setUstProz ( ustProz bgrRow -- )
    8 + :=
;

: bgrRow.update ( aBgr dBgr ustProz bgrRow -- )
    dup -rot bgrRow.setUstProz
    dup -rot bgrRow.setDBgr
    bgrRow.setABgr
;

: bgrRow::new ( aBgr dBgr ustProz -- addr )
    bgrRow::size require
    dup -rot bgrRow.setUstProz
    dup -rot bgrRow.setDBgr
    dup -rot bgrRow.setABgr
;

: bgrRow.delete ( bgrRow -- )
    free
;

: bgrRow.printABgr
    bgrRow.getABgr .
;
: bgrRow.printDBgr
    bgrRow.getDBgr . 
;
: bgrRow.printUstProz
    bgrRow.getUstProz . 
;
: bgrRow.printRow
    " aBgr: " $. dup bgrRow.printABgr ", dBgr: " $. dup bgrRow.printDBgr ", ustProz: " $. bgrRow.printUstProz cr
;


: bgrTable::introSize 4 ;

: bgrTable::new ( count-rows -- addr )
        bgrRow::size * 
        bgrTable::introSize + 
    require 
    dup 0 swap := ( write rowcount)
;


: bgrTable.getRowCount ( bgrTable -- count )
    ?
;

: bgrTable.setRowCount ( count bgrTable -- )
    := 
;

: bgrTable.getRowByIndex ( index bgrTable -- row )
    swap reference::size * + bgrTable::introSize + 
;

: bgrTable.getEnd ( bgrTable -- free-address )
    dup bgrTable.getRowCount swap bgrTable.getRowByIndex
;

: bgrTable.addRow ( bgrRow bgrTable -- )
    dup -rot ( bgrTable bgrRow bgrTable )
    bgrTable.getEnd ( bgrTable newRow free-addr )
    2dup swap ( bgrTable newRow free-addr free-addr newRow ) bgrRow.getUstProz swap bgrRow.setUstProz
    2dup swap bgrRow.getDBgr swap bgrRow.setDBgr
    2dup swap bgrRow.getABgr swap bgrRow.setABgr
    2drop ( bgrTable newRow free-addr -- bgrTable )
    ? 1+ swap :=
;

: bgrTable::init ( -- addr )
    20 bgrTable::new
    1 2 10 bgrRow::new
    dup bgrTable.addRow

    2 2 20 bgrRow::new
    bgrTable.addRow

;
