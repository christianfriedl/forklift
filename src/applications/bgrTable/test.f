#include"../../tests/_library.f"
#include"bgrTable.f"

: test-create-row "test-create-row" current-test
    {{
        1 2 3 bgrRow::new
        dup 0 <> "bgrRow::new returns a value" assert
        dup bgrRow.getABgr 1 = "bgrRow.getABgr returns correct value" assert
        dup bgrRow.getDBgr 2 = "bgrRow.getDBgr returns correct value" assert
        dup bgrRow.getUstProz 3 = "bgrRow.getUstProz returns correct value" assert
        bgrRow.delete
    }} true = "function is stack-neutral" assert
;

: test-set-row-values "test-set-row-values" current-test
    {{
        0 0 0 bgrRow::new
        dup 1 swap bgrRow.setABgr
        dup 2 swap bgrRow.setDBgr
        dup 3 swap bgrRow.setUstProz
        dup bgrRow.getABgr 1 = "bgrRow.getABgr returns correct value" assert
        dup bgrRow.getDBgr 2 = "bgrRow.getDBgr returns correct value" assert
        dup bgrRow.getUstProz 3 = "bgrRow.getUstProz returns correct value" assert
        bgrRow.delete
    }} true = "function is stack-neutral" assert
;

: test-update "test-update" current-test
    {{
        0 0 0 bgrRow::new
        dup 1 2 3
            4 roll
            bgrRow.update
        dup bgrRow.getABgr 1 = "bgrRow.getABgr returns correct value" assert
        dup bgrRow.getDBgr 2 = "bgrRow.getDBgr returns correct value" assert
        dup bgrRow.getUstProz 3 = "bgrRow.getUstProz returns correct value" assert
        bgrRow.delete
    }} true = "function is stack-neutral" assert
;

: test-print-row "test-print-row" current-test
    {{
        1 2 3 bgrRow::new
        bgrRow.printRow
    }} true = "function is stack-neutral" assert
;

: test-create-table "test-create-table" current-test
    {{
        20 bgrTable::new
        dup bgrTable.getRowCount 0 = "rowCount is 0" assert
                dup 
                    bgrTable.getEnd 
                    bgrTable::introSize 
                - 
            -
            0 
        = "end points to after intro" assert
    }} true = "function is stack-neutral" assert
;

: test-add-row "test-add-row" current-test
    {{
        20 bgrTable::new
        dup
        1 2 3 bgrRow::new
        swap dup -rot 
        bgrTable.addRow
        bgrTable.getRowCount 1 = "rowCount is 1 after addRow" assert
    }} true = "function is stack-neutral" assert
;

: test-retrieve-row-by-index "test-retrieve-row-by-index" current-test
    {{
        20 bgrTable::new
        dup 1 2 3 bgrRow::new
        swap dup -rot bgrTable.addRow
        dup 0 swap bgrTable.getRowByIndex
        dup bgrRow.getABgr 1 =
        swap dup bgrRow.getDBgr 2 =
        swap bgrRow.getUstProz 3 =
        and and "1. fetched row is same as added row" assert

        dup 2 4 6 bgrRow::new
        swap dup -rot bgrTable.addRow
        dup 1 swap bgrTable.getRowByIndex
        dup bgrRow.getABgr 2 =
        swap dup bgrRow.getDBgr 4 =
        swap bgrRow.getUstProz 6 =
        and and "2. fetched row is same as added row" assert
        drop
    }} true = "function is stack-neutral" assert
;

: test-remove-row "test-remove-row" current-test
    {{
        "TODO" fail
    }} true = "function is stack-neutral" assert
;

: test-find-row-by-values "test-find-row-by-values" current-test
    {{
        "TODO" fail
    }} true = "function is stack-neutral" assert
;

( testing bgrRow )

test-create-row
test-set-row-values
test-update
test-print-row

( testing bgrTable )

test-create-table
test-add-row
test-retrieve-row-by-index
test-remove-row
test-find-row-by-values


0 end
