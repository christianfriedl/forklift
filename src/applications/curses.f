codeblock App
	imports applications/libcurses.fo
	extends Curses

    variable win

    ( string -- x y )
    : get_centered_pos_xy
            win ? 
            getmaxyx  
        2/                          ( string y x/2)
            rot 
            $len 
            2/                      ( y x/2 strlen/2)
        -
            swap 
        2/                     ( x/2 y/2)
    ;

    ( string -- )
    : centered_addstr
            dup 
            get_centered_pos_xy
        mvaddstr
    ;


    start

    init_library
        initscr 
    win :=
        start_color 
    drop
    clear
    refresh
        "Hello World!" 
    addstr
    refresh
        getch 
    drop
        "Centered Hello!" 
    centered_addstr 
    refresh
        getch 
    drop
    endwin

    end
