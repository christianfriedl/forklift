#ifdef ARCH_IA64
    #define SW 8
#else
#ifdef ARCH_I386
    #define SW 4

    #define pushq pushl
    #define popq popl
    #define xorq xorl
    #define movq movl
    #define subq subl
    #define addq addl
    #define cmpq cmpl
    #define testq testl
    #define incq incl
    #define andq andl
    #define jmpq jmpl

    #define rax eax
    #define rbx ebx
    #define rcx ecx
    #define rdx edx
    #define rsp esp
    #define rbp ebp
    #define rsi esi
    #define rdi edi

#else
    #error "arch.h: no architecture defined (missing Makefile.local?)"
#endif
#endif
